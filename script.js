  /*  1.	Переменные, объявленные при помощи let и const имеют одинаковую область видимости, то есть если они объявлены внутри блока, то и видны только внутри этого блока. Это их общее отличие от var. Кроме этого их общим отличием от переменных созданных при помощи var является то, что они видны только после их объявления а при помощи var  и до их объявления. Отличаются let и const между собой тем, что const (то есть константа) не может быть переопределен, и он должен быть инициализирован сразу же после объявления, иначе будет ошибка (let если будет объявлен но не инициализирован то будет иметь значение undefined но ошибки не будет).
  2.	Причина отказа от использования объявления переменной посредством var заключается в том, что переменная, созданная при помощи var, имеет слишком большую область видимости, чаще всего превышающую ту область, в которой есть необходимость в ее использовании. Вследствие этого может произойти не преднамеренная перезапись этой переменной и по этой причине возможны ошибки, и эту перезапись будет довольно сложно обнаружить. 
*/

let user = prompt("What is your name?");
if (user.length <= 2) {  // name validation
  user = prompt("You did not enter a name, please try again", user);
}

let age = prompt("How old are you?");
let ageFault = age;  //creating a variable to pass incorrect data entered into the "default" field
age = Number(age);

while (Number.isNaN(age)) {     // age validation
  age = +prompt("Are you sure you entered the correct age?", ageFault);
}

if (age < 18) {
  alert("You are not allowed to visit this website");

} else if (age <= 22) {
    const answer = confirm("Are you sure you want to continue?");
    if (answer) {
      alert("Welcome " + user);
    
    } else {
      alert("You are not allowed to visit this website");
    }

} else {
  alert("Welcome " + user);
}
